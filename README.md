# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository contains pages related to Lightning Ready demo in JDF17
* version 1.0

### How do I get set up? ###

* You just need a developer org to try this code, these are mainly visualforce pages with Classic and LEX(SLDS) style.
* Copy and paste each page in your dev org and preview.

### Who do I talk to? ###

* Please tweet me at [prafulgadge](http://twitter.com\prafulgadge). if you have any questions.